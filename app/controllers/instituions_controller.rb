class InstituionsController < ApplicationController
  before_action :set_instituion, only: %i[ show edit update destroy delete]

  # GET /students or /students.json
  def index
    @instituions = Instituion.all
  end

  # GET /students/1 or /students/1.json
  def show
  end

  # GET /students/new
  def new
    @instituion = Instituion.new
  end

  # GET /students/1/edit
  def edit
  end

  # POST /students or /students.json
  def create
    @instituion = Instituion.new(instituion_params)

    respond_to do |format|
      if @instituion.save
        format.html { redirect_to instituions_path, notice: "Instituion was successfully created." }
        format.json { render :show, status: :created, location: @instituion }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @instituion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1 or /students/1.json
  def update
    if @instituion.update(instituion_params)
      respond_to do |format|
        format.html { redirect_to instituions_path, notice: 'Instituion was successfully destroyed.' }
      end
    else
      render json: @instituion.errors, status: 422
    end
  end


  # DELETE /students/1 or /students/1.json

  def destroy
    @instituion.destroy
    respond_to do |format|
      format.html { redirect_to instituions_path, notice: 'Instituion was successfully destroyed.' }
      format.json { head :no_content }
      
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_instituion
      @instituion = Instituion.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def instituion_params
      params.require(:instituion).permit(:name, :cnpj, :type_status)
    end
end
