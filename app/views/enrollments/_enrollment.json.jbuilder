json.extract! enrollment, :id, :total_course_fee, :number_invoice, :expiration_day, :name_course, :student_id, :instituion_id, :created_at, :updated_at
json.url enrollment_url(enrollment, format: :json)
