json.extract! instituion, :id, :name, :cnpj, :type_status, :created_at, :updated_at
json.url instituion_url(instituion, format: :json)
