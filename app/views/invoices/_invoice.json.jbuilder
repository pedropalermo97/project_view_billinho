json.extract! invoice, :id, :invoice_amount, :due_data, :status, :enrollment_id, :created_at, :updated_at
json.url invoice_url(invoice, format: :json)
