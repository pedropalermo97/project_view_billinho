json.extract! student, :id, :name, :cpf, :birth_date, :cellphone, :gender, :payment_method, :created_at, :updated_at
json.url student_url(student, format: :json)
