Rails.application.routes.draw do
  resources :invoices
  resources :enrollments
  resources :instituions
  resources :students


  get 'students/', to: "posts#index"
  root to: "students#index"


  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
