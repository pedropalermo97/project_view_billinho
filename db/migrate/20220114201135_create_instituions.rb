class CreateInstituions < ActiveRecord::Migration[6.1]
  def change
    create_table :instituions do |t|
      t.string :name
      t.string :cnpj
      t.string :type_status

      t.timestamps
    end
  end
end
