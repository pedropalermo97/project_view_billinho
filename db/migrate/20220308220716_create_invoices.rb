class CreateInvoices < ActiveRecord::Migration[6.1]
  def change
    create_table :invoices do |t|
      t.decimal :invoice_amount
      t.date :due_data
      t.string :status
      t.references :enrollment, null: false, foreign_key: true

      t.timestamps
    end
  end
end
