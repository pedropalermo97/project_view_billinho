# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Student.create!(name:"Pedro",cpf:"16489301741",gender:"Masculino",payment_method:"Cartao",birth_date:"18/04/1997",cellphone:"21980117422")
Student.create!(name:"Pedra",cpf:"16489301742",gender:"Masculino",payment_method:"Cartao",birth_date:"18/04/1997",cellphone:"21980117423")
Student.create!(name:"Pedrao",cpf:"16489301743",gender:"Masculino",payment_method:"Cartao",birth_date:"18/04/1997",cellphone:"21980117424")
Student.create!(name:"Pedrinho",cpf:"16489301744",gender:"Masculino",payment_method:"Cartao",birth_date:"18/04/1997",cellphone:"21980117425")
Student.create!(name:"Thiago",cpf:"16489301999",gender:"Masculino",payment_method:"Boleto",birth_date:"19/01/2000",cellphone:"11980119999")
Instituion.create!(name:"UFF",cnpj:"16489301741",type_status:"Universidade")
Instituion.create!(name:"Creche do Papai",cnpj:"11489301749",type_status:"Creche")
Enrollment.create!(total_course_fee:12000.00, number_invoice:12, expiration_day:10, name_course:"ingles", student_id:1, instituion_id:2)
