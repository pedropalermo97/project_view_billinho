require "test_helper"

class InstituionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @instituion = instituions(:one)
  end

  test "should get index" do
    get instituions_url
    assert_response :success
  end

  test "should get new" do
    get new_instituion_url
    assert_response :success
  end

  test "should create instituion" do
    assert_difference('Instituion.count') do
      post instituions_url, params: { instituion: { cnpj: @instituion.cnpj, name: @instituion.name, type_status: @instituion.type_status } }
    end

    assert_redirected_to instituion_url(Instituion.last)
  end

  test "should show instituion" do
    get instituion_url(@instituion)
    assert_response :success
  end

  test "should get edit" do
    get edit_instituion_url(@instituion)
    assert_response :success
  end

  test "should update instituion" do
    patch instituion_url(@instituion), params: { instituion: { cnpj: @instituion.cnpj, name: @instituion.name, type_status: @instituion.type_status } }
    assert_redirected_to instituion_url(@instituion)
  end

  test "should destroy instituion" do
    assert_difference('Instituion.count', -1) do
      delete instituion_url(@instituion)
    end

    assert_redirected_to instituions_url
  end
end
