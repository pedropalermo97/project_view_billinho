require "application_system_test_case"

class InstituionsTest < ApplicationSystemTestCase
  setup do
    @instituion = instituions(:one)
  end

  test "visiting the index" do
    visit instituions_url
    assert_selector "h1", text: "Instituions"
  end

  test "creating a Instituion" do
    visit instituions_url
    click_on "New Instituion"

    fill_in "Cnpj", with: @instituion.cnpj
    fill_in "Name", with: @instituion.name
    fill_in "Type status", with: @instituion.type_status
    click_on "Create Instituion"

    assert_text "Instituion was successfully created"
    click_on "Back"
  end

  test "updating a Instituion" do
    visit instituions_url
    click_on "Edit", match: :first

    fill_in "Cnpj", with: @instituion.cnpj
    fill_in "Name", with: @instituion.name
    fill_in "Type status", with: @instituion.type_status
    click_on "Update Instituion"

    assert_text "Instituion was successfully updated"
    click_on "Back"
  end

  test "destroying a Instituion" do
    visit instituions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Instituion was successfully destroyed"
  end
end
